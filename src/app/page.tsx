
export default function Home() {

  const testimonialData = [
    {
      content:
        "Listening to Spencer talk on stage at the Content Creators Conference today was amazing!! Cant wait to get hold of a copy of his book – left me inspired for the next stage in launching my own book and ways I can help others. We all have a story and a beast to unleash within us:)",
      author: 'Nilam Shah',
      image: 'dummy_profile_1',
    },
    {
      content:
        "Spencer is a fantastic leader and amazing human being. There is always great value he adds to our company work. Every interaction with Spencer brings all of us to another level of growth as professionally so as personally. Great sense of humor and must do attitude always inspire us to become, do and give more today than we did yesterday. Thanks a million, Spencer and looking forward to working with you more! 🙏🙂👍",
      author: 'Lana Diditska',
      image: 'dummy_profile_2',
    },
    {
      content:
        'You give a very outstanding idea about how to make anything happened. Your words are really motivating. Hope to meet you eagerly. Thank you giving outstanding idea.😊😊😊',
      author: 'Zakir Hossain Marofe',
      image: 'dummy_profile_3',
    },
    {
      content:
        'I actually followed Spencer just recently and I without exaggerating, learned a lot from him and I just downloaded all of his ebooks eight of them actually and I will read them and tell you guys about them soon. Hope you have a 10x day ❤️',
      author: 'Ahmed Almansouri',
      image: 'dummy_profile_4',
    },
  ];

  const latestFromBlog = [
    {
      image: 'blog_image_1',
      title: 'Spencer Lodge | April 29, 2020',
      subject: 'HOW TO FIND TIME TO DO EVERYTHING YOU WANT TO',
      content: 'Chances are, you have complained about not having enough time at least once. There are only so many hours a day, right? WRONG. Everyone has'
    },
    {
      image: 'blog_image_2',
      title: 'Spencer Lodge | April 29, 2020',
      subject: 'ARE UNIVERSITIES STILL THE BEST PLACES TO LEARN?',
      content: 'As the lockdown and quarantine slowly take over our lives, many of us are considering using this downtime to upskill. A lot of people have'
    },
    {
      image: 'blog_image_3',
      title: 'Spencer Lodge | April 29, 2020',
      subject: 'ARE UNIVERSITIES STILL THE BEST PLACES TO LEARN?',
      content: 'As of right now, I am sure that there isn’t a single person who doesn’t know about the novel coronavirus, and that it can be'
    },
    {
      image: 'blog_image_4',
      title: 'Spencer Lodge | April 29, 2020',
      subject: 'POSITIVITY – YOUR BEST WEAPON IN A CRISIS',
      content: 'We find ourselves in the midst of a pandemic, which is changing life as we know it. Most of us are staying home, maintaining physical'
    },
  ];

  return (
    <main className="flex justify-center">

      <div className="max-w-[1600px]">
        <div className="flex justify-between items-center nav_background px-20 py-2">

          <div className="flex gap-8">
            <div className="flex items-center gap-2">
              <img src="/phone.svg" />
              <div>+971 50 556 5063</div>
            </div>
            <div className="flex items-center gap-2">
              <img src="/mail.svg" />
              <div>
                sl@make-it-happen.com
              </div>
            </div>
          </div>

          <div className="flex gap-3">
            <img className="cursor-pointer" src="/facebook.svg" />
            <img className="cursor-pointer" src="/instagram.svg" />
            <img className="cursor-pointer" src="/linkedin.svg" />
            <img className="cursor-pointer" src="/youtube.svg" />
          </div>

        </div>

        <div className="relative w-full">
          <img className="absolute w-full z-[-10]" src="/bg_1.svg" />
        </div>

        <div className="relative">
          <div className="flex justify-between items-center absolute text-white w-full px-20 pt-5">
            <div className=" text-xs font-medium">
              <div>T H E</div>
              <div>S P E N C E R</div>
              <div>L O D G E</div>
              <div>P O D C A S T</div>
            </div>
            <div className="flex gap-8">
              <div className="py-2 cursor-pointer">Home</div>
              <div className="py-2 cursor-pointer">Podcast</div>
              <div className="py-2 cursor-pointer">Speaking</div>
              <div className="py-2 cursor-pointer">Consulting</div>
              <div className="py-2 cursor-pointer">Bio & Media</div>
              <div className="py-2 cursor-pointer">About</div>
              <div className="border border-white rounded-full px-4 py-2 cursor-pointer">Contact Us</div>
            </div>
          </div>

          <img src="/home_img_1.svg" />
        </div>

        <div className="px-24 flex justify-between relative pb-[200px] ">
          <img className="w-[64%] absolute top-[-150px] left-0" src="/home_img_3.svg" />
          <img className="w-full absolute bottom-0 left-0" src="/home_img_5.svg" />
          <div className="flex-1 pt-[400px]" >

            <div className="flex items-center gap-1">
              <img className="h-[60px]" src="/i.svg" />
              <img className="h-[120px]" src="/home_img_4.svg" />
            </div>

            <div className="pt-[30px] pr-10 font-sofia-pro text-lg font-normal leading-[38px] tracking-normal text-left">
              With a storied career spanning
              <span className="font-sofia-pro-medium text-lg font-semibold leading-[38px] tracking-normal">
                over three decades in the international financial services and sales industries,
              </span>
              Spencer has left an unforgettable mark.
              <span className="font-sofia-pro-medium text-lg font-semibold leading-[38px] tracking-normal">
                After building formidable multicultural sales forces around the world
              </span>

              and winning many accolades,

              <span className="font-sofia-pro-medium text-lg font-semibold leading-[38px] tracking-normal">
                he's now pursuing his true passion: empowering individuals to reach their fullest potential, in a comprehensive manner.
              </span>

              <br></br>
              <br></br>
              Fueled by unwavering determination and drive,

              <span className="font-sofia-pro-medium text-lg font-semibold leading-[38px] tracking-normal">
                Spencer's pursuit inspired him to create The Spencer Lodge Podcast,
              </span>

              one of the most listened-to podcasts in the region. Each episode is

              <span className="font-sofia-pro-medium text-lg font-semibold leading-[38px] tracking-normal">
                designed to educate and captivate,
              </span>

              as he engages in unforgettable conversations with notable figures, influential experts, and provocative thought leaders. Together, they uncover untold truths, recount remarkable stories, and share invaluable lessons. Follow the conversation as Spencer

              <span className="font-sofia-pro-medium text-lg font-semibold leading-[38px] tracking-normal">
                inspires audiences to lead purposeful lives that transcend traditional notions of achievement.
              </span>
            </div>

          </div>
          <img className="w-3/6" src="/home_img_2.svg" />

        </div>

        <div className="px-24 flex justify-between">

          <div className="pt-20">
            <div className="font-serif italic text-6xl font-normal leading-[81px] tracking-wider text-left">
              Subscribe To the <br />
              Podcast Now
            </div>
            <div className="font-sofia-pro text-base font-normal leading-6 tracking-normal text-left max-w-[500px] py-5">
              Spencer lodge has helped thousands of people find their motivation, break through their fears, and achieve massive success. you can be next!
            </div>
            <div className="flex items-center justify-between gap-2 bg-[#2C3640] w-fit rounded-full px-3 py-2 text-white">
              <img src="/listen.svg" />
              <div className="cursor-pointer">listen now</div>
            </div>
          </div>

          <div className="text-center">
            <div className="font-serif italic text-xl font-normal leading-10 tracking-tighter">Spencer is on</div>
            <div className="font-sofia-pro-medium text-xl font-semibold leading-10 tracking-tighter">Your favorite streaming services</div>
            <div className="flex justify-content gap-4 pt-4">
              <img src="/stream_service_1.svg" />
              <img src="/stream_service_2.svg" />
              <img src="/stream_service_3.svg" />
            </div>
            <div className="flex justify-content gap-4 pt-4">
              <img src="/stream_service_4.svg" />
              <img src="/stream_service_5.svg" />
              <img src="/stream_service_6.svg" />
            </div>
          </div>
        </div>

        <div className="relative w-full">
          <img className="absolute w-full z-[-10]" src="/bg_1.svg" />
        </div>

        <div className="flex justify-center py-10">
          <img src="/home_img_6.svg" />
        </div>

        <div className="flex gap-7 px-[10%]">
          {
            testimonialData.map((testimonial, index) => (
              <div key={index} className="bg-[#F8F8F8] px-6 py-6 flex-1 flex flex-col justify-between">

                <div>
                  <img className="py-5" src="/quote.svg" alt="quote" />
                  <div>{testimonial.content}</div>
                </div>


                <div className="pt-20 flex items-center gap-5">
                  <img className="py-5" src={`/${testimonial.image}.svg`} alt="profile" />
                  <div>
                    <div className="font-sofia-pro text-base font-normal leading-6 tracking-normal text-left">
                      {testimonial.author}
                    </div>
                    <img src="/stars.svg" alt="stars" />
                  </div>
                </div>
              </div>
            ))
          }
        </div>

        <div className="flex justify-center py-10">
          <img src="/home_img_7.svg" />
        </div>

        <div className="flex gap-7 px-[10%]">
          {
            latestFromBlog.map((item, index) => (
              <div key={index} className="bg-[#F8F8F8] px-6 py-6 flex-1 flex flex-col justify-between">



                <div>
                  <img className="py-5" src={`/${item.image}.svg`} />
                  <div className="font-sofia-pro text-xs font-normal leading-5 tracking-normal">{item.title}</div>
                  <div className="font-sofia-pro text-base font-medium leading-7 tracking-normal">{item.subject}</div>
                  <div className="font-sofia-pro text-base font-normal leading-6 tracking-normal">{item.content}</div>
                </div>


                <div className="pt-5 flex items-center justify-center gap-5">
                  <div className="font-sofia-pro-medium text-base font-medium leading-6 tracking-normal text-center cursor-pointer">
                    Read the full Article
                  </div>
                </div>
              </div>
            ))
          }
        </div>

        <div className="flex w-full px-[10%] py-10">

          <div className="flex-[3]">
            <div className="font-serif italic text-7xl font-normal leading-[91px] tracking-tighter">
              Contact
            </div>
            <div className="font-sofia-pro-medium text-7xl font-medium leading-[91px] tracking-tighter">
              Spencer Now
            </div>

            <div className="font-serif italic text-3xl font-normal leading-11 tracking-tighter py-3">
              Info
            </div>
            <div className="flex items-start gap-2">
              <img src="/location.svg" />
              <div className="font-inter text-base font-normal leading-[29px] tracking-normal">
                Make It Happen FZ LLCPalm Jumeirah, PO BOX <br /> 923018DubaiUnited Arab Emirates
              </div>
            </div>
            <div className="flex gap-2">
              <img src="/phone.svg" />
              <div className="font-inter text-base font-normal leading-[29px] tracking-normal">
                +971 50 556 5063
              </div>
            </div>

            <div className="font-inter text-lg font-bold leading-[27px] tracking-normal  py-3">Spencer Lodge</div>
            <div className="leading-[27px]">sl@make-it-happen.com</div>
            <div className="font-inter text-lg font-bold leading-[27px] tracking-normal py-3">Media Inquiries</div>

            <div className="flex w-full justify-between pr-10">
              <div>
                <div className="font-inter text-base font-semibold leading-[22px] tracking-normal">
                  Middle East & USA:
                </div>
                <div>
                  Sofia De MaussionManaging Director
                </div>
                <div>
                  sdm@make-it-happen.com
                </div>
              </div>
              <div>
                <div className="font-inter text-base font-semibold leading-[22px] tracking-normal">
                  UK & rest of the world:
                </div>
                <div>
                  Samantha WilkinsPR & Communications
                </div>
                <div>
                  samantha@talentandtruth.com
                </div>
              </div>
            </div>



          </div>

          <div className="rounded-lg border border-solid border-1 flex-[2] bg-white">
            <div className="px-8 py-6 flex flex-col gap-3">
              <div className="font-inter text-base font-bold leading-5 tracking-normal">
                Take the First Step
              </div>
              <div className="font-serif italic text-2xl font-normal leading-10 tracking-normal text-left">
                Get in touch with
                <span className="font-inter text-lg font-bold leading-7 tracking-normal"> Us</span>
              </div>
              <div className="rounded-lg w-full border border-solid border-gray-300 rounded-5 ">
                <input className="px-4 py-2 rounded-lg w-full" placeholder="Your name"></input>
              </div>
              <div className="rounded-lg w-full border border-solid border-gray-300 rounded-5 ">
                <input className="px-4 py-2 rounded-lg w-full" placeholder="Your name"></input>
              </div>
              <div className="rounded-lg w-full border border-solid border-gray-300 rounded-5 ">
                <input className="px-4 py-2 rounded-lg w-full" placeholder="Your name"></input>
              </div>
              <div className="rounded-lg w-full border border-solid border-gray-300 rounded-5 ">
                <input className="px-4 py-2 rounded-lg w-full" placeholder="Your name"></input>
              </div>
              <div className="rounded-lg w-full border border-solid border-gray-300 rounded-5 ">
                <input className="px-4 py-2 rounded-lg w-full" placeholder="Your name"></input>
              </div>

              <div>
                <img src="/are_you_robot.svg" />

              </div>

              <div className="rounded-lg text-white bg-[#2C3640] py-2 text-center cursor-pointer">Submit</div>

            </div>

          </div>

        </div>


        <div className="flex px-[10%] relative pt-[180px]">
          <img className="w-full absolute left-0 top-0" src="/home_img_8.svg" />

          <div className="flex-1 pt-[100px] flex flex-col justify-between pb-10">
            <div>
              <img className="py-8" src="/home_img_10.svg" />
              <div className="flex flex-col gap-5">
                <div className="flex items-center gap-2 bg-[#C7FF00] w-fit px-3 py-2 rounded-[30px]">
                  <img src="home_img_11.svg" />
                  <span className="text-sm leading-[25px] tracking-normal">
                    THE <span className="font-semibold">MOST POPULAR</span> PODCAST
                    <span className="font-semibold">IN THE  MIDDLE EAST</span>
                  </span>
                </div>
                <div className="text-3xl font-normal leading-[49px] tracking-normal">
                  <span className="font-semibold">AWARD-WINNING </span>
                  PODCAST HOST & <br /> BUSINESS STRATEGIST VOTED TOP 100 MOST <br /> INFLUENTIAL PEOPLE IN DUBAI
                </div>
                <div className="flex items-center gap-4">
                  <div className="flex items-center gap-2 bg-[#A7B5C3] w-fit px-3 py-2 rounded-[30px] bg-opacity-30">
                    <img src="/home_img_12.svg" />
                    <div>The Spencer Lodge Podcast on Spotify</div>
                  </div>
                  <div className="font-semibold">
                    {'>'} Consulting
                  </div>
                </div>
              </div>



            </div>

            <div>

              <div className="flex gap-3 py-5">
                <img className="cursor-pointer" src="/facebook.svg" />
                <img className="cursor-pointer" src="/instagram.svg" />
                <img className="cursor-pointer" src="/linkedin.svg" />
                <img className="cursor-pointer" src="/youtube.svg" />
              </div>

              <div>
                COPYRIGHT 2023 - SPENCER LODGE / MAKE IT HAPPEN FZ LLC
              </div>
            </div>
          </div>

          <img className="w-[35%]" src="/home_img_9.svg" />

        </div>
      </div>

    </main>
  )
}
